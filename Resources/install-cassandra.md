# Install Cassandra

## 1. System Update and Upgrade
```bash
sudo apt update
# update the local repo
sudo apt upgrade
# upgrade required packages
# sudo apt install cassandra - give an error
```

## 2. Adding Cassandra repos to APT
```bash
# Add Cassandra servers keys
echo "deb http://www.apache.org/dist/cassandra/debian 40x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list
# Add Cassandra servers urls
wget -q -O - https://www.apache.org/dist/cassandra/KEYS | sudo tee /etc/apt/trusted.gpg.d/cassandra.asc
# Update the apt tool to consider th Cassandra repos
sudo apt update
```

## 3. Install Cassandra using APT
```bash
sudo apt install cassandra
```

## 4. Verfiy the installation and connect to the node
```bash
# check cassandra service status
sudo systemctl status cassandra
```
```bash
sudo nodetool status
# provides information about the current node
# node up - cluster name - datacenter - host id
```

```bash
# Connect to the DB and run quries
cqlsh
# CQL : Cassandra QL
```